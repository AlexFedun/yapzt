using Allure.Commons;
using NUnit.Allure.Attributes;
using NUnit.Allure.Core;
using NUnit.Framework;
using NUnit.Framework.Interfaces;
using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;
using OpenQA.Selenium.Interactions;
using OpenQA.Selenium.Remote;
using System;

namespace LR_4
{
    public class Base
    {
        public static IWebDriver driver = new ChromeDriver("driver");

        [SetUp]
        public void Setup()
        {
            driver.Url = "http://the-internet.herokuapp.com";
            var desiredCapabilities = new DesiredCapabilities();
            desiredCapabilities.Platform = new Platform(PlatformType.Any);
            desiredCapabilities.SetCapability("BrowserName", "chrome");
            desiredCapabilities.SetCapability("BrowserVersion", "83.0");
            desiredCapabilities.SetCapability("enableVNC", true);
            driver = new RemoteWebDriver(new Uri("http://localhost:4444/wd/hub"), desiredCapabilities);
        }

        [TearDown]
        public void Teardown()
        {
            if (TestContext.CurrentContext.Result.Outcome != ResultState.Success)
            {
                string path = @"E:\������\����������\Lab_3\results\";
                var imagename = $"results_{DateTime.Now:yyyy-MM-dd_HH-mm-ss.fffff}.png";
                var screenshot = ((ITakesScreenshot)driver).GetScreenshot();
                screenshot.SaveAsFile(path + imagename);
            }

            driver.Quit();
        }

    }

    public static class Helpers
    {
        public static IWebDriver driver = Base.driver;
        public static bool acceptNextAlert = true;

        public static void OpenPage(string url)
        {
            driver.Navigate().GoToUrl(url);
        }

        public static bool IsElementPreset(By by)
        {
            return driver.FindElement(by).Enabled;
        }

        public static bool IsAlertPresent()
        {
            try
            {
                driver.SwitchTo().Alert();
                return true;
            }
            catch (NoAlertPresentException Ex)
            {
                return false;
            } 
        }

        public static string CloseAlertAndGetItsText()
        {
            try
            {
                return driver.SwitchTo().Alert().Text;
            }
            catch (NoAlertPresentException Ex)
            {
                return "";
            }
        }
    }

    [TestFixture]
    [AllureNUnit]
    public class TestCases : Base
    {
        [Test]
        [AllureStory]
        [AllureTag("NUnit")]
        public void TestMethod()
        {
            Helpers.OpenPage("google.com");
            ((ITakesScreenshot)driver).GetScreenshot();
            AllureLifecycle.Instance.AddAttachment(@"E:\������\����������\Lab_4\Allure\");
        }

        [Test]
        [Category("Faker"), Description("This test make fake data")]
        public void MultipleWindowsTest()
        {
            driver.Navigate().GoToUrl("https://rozetka.com.ua/");

            string email = Faker.User.Email();
            string password = Faker.User.Password();

            driver.FindElement(By.CssSelector(".header-topline__user-link")).Click();
            driver.FindElement(By.CssSelector(".auth_email")).SendKeys(email);
            driver.FindElement(By.CssSelector(".auth_pass")).SendKeys(password);
            driver.FindElement(By.CssSelector(".auth-modal__submit")).Click();
        }
 
    }
}