using NUnit.Framework;
using NUnit.Framework.Interfaces;
using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;
using OpenQA.Selenium.Interactions;
using System;

namespace LR_3
{
    public class Base
    {
        public IWebDriver driver = new ChromeDriver("driver");

        [SetUp]
        public void Setup()
        {
            driver.Url = "http://the-internet.herokuapp.com";
        }

        [TearDown]
        public void Teardown()
        {
            if (TestContext.CurrentContext.Result.Outcome != ResultState.Success)
            {
                string path = @"E:\������\����������\Lab_3\results\";
                var imagename = $"results_{DateTime.Now:yyyy-MM-dd_HH-mm-ss.fffff}.png";
                var screenshot = ((ITakesScreenshot)driver).GetScreenshot();
                screenshot.SaveAsFile(path + imagename);
            }

            driver.Quit();
        }

    }

    public class TestCases : Base
    {
        [Test]
        public void MultipleWindowsTest()
        {
            driver.Navigate().GoToUrl("http://the-internet.herokuapp.com/windows");
            driver.FindElement(By.CssSelector(".example a")).Click();
            var allWindows = driver.WindowHandles;
            driver.SwitchTo().Window(allWindows[0].ToString());
            Assert.That(!driver.Title.Equals("New Window"));
            driver.SwitchTo().Window(allWindows[1].ToString());
            Assert.That(driver.Title.Equals("New Window"));
        }
        [Test]
        public void KeyPressesTest()
        {
            driver.Navigate().GoToUrl("http://the-internet.herokuapp.com/key_presses");
            Actions builder = new Actions(driver);
            builder.SendKeys(Keys.Backspace).Build().Perform();
            Assert.That(driver.FindElement(By.Id("result")).Text.Equals("You entered: BACK_SPACE"));
        }

        [Test]
        public void InputsNumber()
        {
            driver.Navigate().GoToUrl("http://the-internet.herokuapp.com/inputs");
            driver.FindElement(By.TagName("input")).Click();
            Actions builder = new Actions(driver);
            builder.SendKeys("1").Build().Perform();
            Assert.That(driver.FindElement(By.TagName("input")).GetProperty("value").Equals("1"));
            Actions builder2 = new Actions(driver);
            builder2.SendKeys("A").Build().Perform();
            Assert.That(driver.FindElement(By.TagName("input")).GetProperty("value").Equals("1"));
        }

        [Test]
        public void Typo()
        {
            driver.Navigate().GoToUrl("http://the-internet.herokuapp.com/typos");
            Assert.That(driver.FindElement(By.CssSelector(".example p:last-child")).Text.Equals("Sometimes you'll see a typo, other times you won't."));
        }

        [Test]
        public void Auth()
        {
            driver.Navigate().GoToUrl("https://admin:admin@the-internet.herokuapp.com/basic_auth");
            Assert.That(driver.FindElement(By.TagName("h3")).Text.Equals("Basic Auth"));
        }

        [Test]
        public void AddRemoveElements()
        {
            driver.Navigate().GoToUrl("https://the-internet.herokuapp.com/add_remove_elements/");
            driver.FindElement(By.TagName("button")).Click();
            Assert.That(driver.FindElement(By.CssSelector(".added-manually")).Enabled.Equals(true));
            driver.FindElement(By.CssSelector(".added-manually")).Click();
        }

        [Test]
        public void CheckTheUncheckedboxTest()
        {
            driver.Navigate().GoToUrl("http://the-internet.herokuapp.com/checkboxes");
            driver.FindElement(By.CssSelector("#checkboxes input")).Click();
            Assert.That(driver.FindElement(By.CssSelector("#checkboxes input")).GetAttribute("checked") == "true");
        }

        [Test]
        public void HoverTest()
        {
            driver.Navigate().GoToUrl("http://the-internet.herokuapp.com/hovers");
            Actions actions = new Actions(driver);
            actions.MoveToElement(driver.FindElement(By.CssSelector("figure")));
            Assert.AreEqual(driver.FindElement(By.CssSelector("figcaption")).Displayed, true);
        }

        [Test]
        public void HandlingContextmenus()
        {
            driver.Navigate().GoToUrl("http://the-internet.herokuapp.com/context_menu");
            Actions actions = new Actions(driver);
            actions.MoveToElement(driver.FindElement(By.CssSelector("figure")));
            Assert.AreEqual(driver.FindElement(By.CssSelector("figcaption")).Displayed, true);

            Actions doAction = new Actions(driver);
            doAction.ContextClick(driver.FindElement(By.Id("hot-spot"))).SendKeys(Keys.Down)
                    .SendKeys(Keys.Return).Build().Perform();

            Assert.That(driver.FindElement(By.CssSelector("#content h2")).Text.Equals("Available Examples"));
        }

        [Test]
        public void DynamicContent()
        {
            driver.Navigate().GoToUrl("http://the-internet.herokuapp.com/dynamic_content");
            var text = driver.FindElement(By.XPath("//div[@class='large-10 columns']")).Text;

            driver.Navigate().GoToUrl("http://the-internet.herokuapp.com/dynamic_content");
            Assert.AreNotEqual(driver.FindElement(By.XPath("//div[@class='large-10 columns']")).Text, text);
        }
    }
}